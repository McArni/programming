package it.buler.programming.xmass;

import java.util.Random;

public class XmassTreeDrawer {

    public static void drawTreeLevel(int maxSize, int spaceCount, char[] decorations, boolean isTop) {
        int treeSize = getTreeSize(maxSize, isTop);
        spaceCount+=getSpaceCountCorrection(maxSize, isTop);

        Random random = new Random();
        while (treeSize <= maxSize) {
            for (int idx = 0; idx < spaceCount; idx++) {
                System.out.print(" ");
            }
            for (int idx = 0; idx < treeSize; idx++) {
                if (random.nextInt(100) < 75) {
                    System.out.print("\u001B[46m" + "x" + "\u001B[0m");
                } else {
                    int color = random.nextInt(8);
                    System.out.print("\u001B[4" + color + "m" + decorations[random.nextInt(decorations.length)] + "\u001B[0m");
                }
            }
            System.out.print("\n");
            spaceCount--;
            treeSize += 2;
        }
    }

    private static int getSpaceCountCorrection(int maxSize, boolean isTop) {
        if (isTop) {
            return 0;
        } else if (maxSize <= 11) {
            return -1;
        } else if (maxSize <= 21) {
            return -2;
        }
        return -3;
    }

    private static int getTreeSize(int maxSize, boolean isTop) {
        if (isTop) {
            return 1;
        } else if (maxSize <= 11) {
            return 3;
        } else if (maxSize <= 21) {
            return 5;
        }
        return 7;
    }
}
