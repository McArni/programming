package it.buler.programming.xmass;

import java.util.Scanner;

public class XmassTreeApplication {

    public static void main(String[] args) {
        int size;

        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj rozmiar podstawy choinki (tylko liczby nieparzyste i wieksze badz rowne 3)");
        size = scanner.nextInt();
        while(!(size >=3 && size % 2 != 0)){
            if(size < 3){
                System.out.println("Tylko liczby wieksze badz rowne 3!");
            }
            if(size % 2 == 0){
                System.out.println("Tylko liczby nieparzyste!");
            }
            size = scanner.nextInt();
        }

        int treeSize = 1;
        int spaceCount = size / 2 + 2;


        drawTreeLevel(treeSize, size, spaceCount);

        drawTreeLevel(treeSize, size + 2, spaceCount);

        drawTreeLevel(treeSize, size + 4, spaceCount);
    }

    public static void drawTreeLevel(int treeSize, int maxSize, int spaceCount) {
        while (treeSize <= maxSize) {
            for (int idx = 0; idx < spaceCount; idx++) {
                System.out.print(" ");
            }
            for (int idx = 0; idx < treeSize; idx++) {
                System.out.print("x");
            }
            System.out.print("\n");
            spaceCount--;
            treeSize += 2;
        }
    }
}

