package it.buler.programming.xmass;

import java.util.Random;
import java.util.Scanner;

import static it.buler.programming.xmass.XmassTreeDrawer.drawTreeLevel;

public class XmassTreeApplicationV2 {

    public static void main(String[] args) {
        int size;
        char[] decorations = {'o', 's', 'c'};

        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj rozmiar podstawy choinki (tylko liczby nieparzyste i wieksze badz rowne 3)");
        size = scanner.nextInt();

        if (size < 3 || size % 2 == 0 || size > 39) {
            throw new IllegalArgumentException("Czytaj ze zrozumieniem!");
        }

        int spaceCount = size / 2 + 2;

        drawTreeLevel(size, spaceCount, decorations, true);

        drawTreeLevel(size + 2, spaceCount, decorations, false);

        drawTreeLevel(size + 4, spaceCount, decorations, false);
    }


}
